//wrapper to manage user input.

#include "simHH/includes/simHH.h"
#include <iostream>

int main(int argc, char *argv[]) {
    if(argc>1){
        for (int i = 0; i < argc; i++)
           std::cout << "argument " << i << " : " << argv[i] << "\n";
    }
    if(argc > 2) return 0;
    if(argc < 2){
        return simHH("./configuration/test.json");
    }

    return simHH(argv[1]);
}
