//
// Created by max on 22-03-21.
//

#ifndef MGPUHH_V2_CPU_KERNELS_H
#define MGPUHH_V2_CPU_KERNELS_H

#include "common.h"
#include "Modular_sim_object.h"
#include <string.h>

bool fn_init_cpu_Backend(SimRunInfo &sim,process_info &ProcessInfo,Network_state &NetworkState_l,NetworkConstStruct *NetworkConst_l);
bool RunTimestep_cpu(size_t step, SimRunInfo &sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l);
bool fn_copy_back_dev_to_host_cpu(SimRunInfo sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l);

void fn_copy_host_to_dev_cpu(void* dev, void* host, size_t size);

#endif //MGPUHH_V2_CPU_KERNELS_H
