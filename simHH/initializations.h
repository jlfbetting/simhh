//
// Created by max on 23-03-21.
//

#ifndef MGPUHH_V2_INITIALIZATIONS_H
#define MGPUHH_V2_INITIALIZATIONS_H

#include "common.h"
#include "Modular_sim_object.h"

bool fn_cellStructure_Initializations(SimRunInfo sim,NetworkConstStruct *Network_l_host,CellConstStruct *cellConstStruct, CompartmentConstStruct *compartmentConstants_dev, ChannelConstStruct *channelConstants_dev,GateConstStruct *gateConstants_dev, size_t fromCellID, size_t aCellID);
bool Initialize_Balanced_constant_state(SimRunInfo sim, process_info ProcessInfo,NetworkConstStruct &Network_l,Network_state &NetworkState_l);

#endif //MGPUHH_V2_INITIALIZATIONS_H
