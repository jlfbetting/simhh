//
// Created by max on 22-03-21.
//

#ifndef MGPUHH_V2_JSON_H
#define MGPUHH_V2_JSON_H

//define includes
#include "thirdparty/cJson-1.7.1/cJSON.h"

#include "common.h"
#include "Modular_sim_object.h"


//functions
bool ReadSimRunDefinition(const char *filename, SimRunInfo &sim,process_info ProcessInfo);
bool WriteMetaData_json(FILE *fout, const RunMetaData &md);

#endif //MGPUHH_V2_JSON_H
