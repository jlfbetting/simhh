//
// Created by max on 07-05-21.
//

#include "memory.h"
#include "sys/types.h"
#include "sys/sysinfo.h"

#include "stdio.h"
#include "string.h"

size_t get_memory_total_cpu() { //inkb
    struct sysinfo memInfo{};
    sysinfo (&memInfo);
    size_t totalVirtualMem = memInfo.totalram;
    totalVirtualMem += memInfo.totalswap; //Add other values in next statement to avoid int overflow on right hand side...
    totalVirtualMem *= memInfo.mem_unit;
    return totalVirtualMem/1000;
}

size_t parseLine(char* line){ //inkb
    // This assumes that a digit will be found and the line ends in " Kb".
    size_t i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}
size_t get_memory_cpu_current(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    size_t result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}