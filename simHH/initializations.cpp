//TODO:
// -- padding of the balancer :D

//
// Created by max on 22-03-21.
//

#include "initializations.h"
#include "miniLogger/miniLogger.h"

/* HELPER function to create structures*/
CellConstStruct newCellConstStruct(uint32_t compsize,  CompartmentConstStruct *compartmentConstants){
    CellConstStruct c;
    c.compartment = compartmentConstants;
    c.NeighCount = 0;
    c.NeighIds = nullptr;
    c.NeighCondu = nullptr;
    return c;
}
CompartmentConstStruct newCompartmentConstStruct(float S, float vLeak, float gLeak, float gInt,float gp,uint32_t identifier, uint32_t nChannels, ChannelConstStruct *Channel,bool enable_calc,GateConstStruct CalciumCon) {
    CompartmentConstStruct c;
    c.S = S;
    c.vLeak = vLeak;
    c.gLeak = gLeak;
    c.gInt = gInt;
    c.gp = gp;
    c.identifier = identifier;  //for now 0 == start cell; 1 == endcell; 2 = inbetween; 3 = single compartment cell
    c.nChannels = nChannels;

    if(Channel == nullptr) std::cout << "ERRROR\n";
    c.Channel = Channel;

    c.enable_CalciumConcentration = enable_calc;
    c.CalciumConcentration = CalciumCon;
    return c;
}
ChannelConstStruct newChannelConstStruct(bool is_ca2plus_channel, uint32_t nGates, GateConstStruct *Gate) {
    ChannelConstStruct c;
    c.is_ca2plus_channel = is_ca2plus_channel;

    c.nGates = nGates;
    c.Gate = Gate;
    return c;
}
GateConstStruct newGateConstStruct(float aX[9],float bX[9],uint8_t aFtype,uint8_t bFtype,float p) {
    GateConstStruct c;
    c.bFtype = bFtype;
    c.bFtype = bFtype;
    c.aFtype = aFtype;
    for(int i = 0; i<9; i++) {
        c.aX[i] = aX[i];
        c.bX[i] = bX[i];
    }
    c.p = p;
    return c;
}
void FormulaToXsN(const Formula &fCustom, float x[9]){
    switch(fCustom.type){
        case Formula::LINEAR_BY_EXPONENTIAL: {
            const Formula::LinByExp &f = fCustom.lin_by_exp;
            x[0] = f.exp_scale;
            x[1] = -f.exp_var_offset;
            x[2] = -f.exp_var_scale;
            x[3] = f.exp_offset;
            x[4] = 0; //unused
            x[5] = -f.lin_scale;
            x[6] = -f.lin_offset;
            x[7] = 0; //unused
            x[8] = f.offset;
            break; }
        case Formula::INVERSE_OF_TWO_EXPONENTIALS: {
            const Formula::InvOfTwoExp &f = fCustom.inv_of_two_exp;
            x[0] = f.exp1_scale;
            x[1] = -f.exp1_var_offset;
            x[2] = -f.exp1_var_scale;
            x[3] = f.den_offset;
            x[4] = f.exp2_scale;
            x[5] = -f.exp2_var_scale;
            x[6] = -f.exp2_var_offset;
            x[7] = 0; //unused
            x[8] = f.offset;
            break; }
        case Formula::EXPONENTIAL_BY_EXPONENTIAL: {
            const Formula::ExpByExp &f = fCustom.exp_by_exp;
            x[0] = f.num_scale;
            x[1] = -f.num_var_offset;
            x[2] = -f.num_var_scale;
            x[3] = f.num_offset;
            x[4] = f.den_scale;
            x[5] = -f.den_var_scale;
            x[6] = -f.den_var_offset;
            x[7] = f.den_offset;
            x[8] = f.offset;
            break; }
        case Formula::CLIPPED_LINEAR: {
            const Formula::ClippedLinear &f = fCustom.clipped_linear;
            x[0] = f.maximum;
            x[1] = -f.offset;
            x[2] = -f.scale;
            x[3] = 0; //unused
            x[4] = 0; //unused
            x[5] = 0; //unused
            x[6] = 0; //unused
            x[7] = 0; //unused
            x[8] = 0; //unused
            break; }
        default:
            INIT_LOG(LOG_DEFAULT);
            log(LOG_ERR) << "UNKOWN type fCustom " << fCustom.type;
            break;
    }
}

//todo explain
bool fn_network_balancer(const SimRunInfo sim,process_info ProcessInfo, NetworkConstStruct &networkConstStruct) {
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);
    log(LOG_DEBUG,0) << "the balancer lets go its stupidly simple and not inteligent" << LOG_ENDL;

    //double checko
    if (sim.population.type != CellPopulationInfo::SET) {
        log(LOG_ERR) << "UNSET population type" << LOG_ENDL;
        exit(3);
        return false;
    }

    //check if we can spread evenly todo
    const size_t nCells_tmp = sim.population.population_size;
    if (nCells_tmp % ProcessInfo.world_size != 0) {
        log(LOG_ERR) << "The balancer can't evenly spread the number off cells, TODO to add padding :D" << LOG_ENDL;
        exit(2);
        return false;
    }

    //set local ncells
    networkConstStruct.nCells  = nCells_tmp;
    networkConstStruct.nCells_l = nCells_tmp / ProcessInfo.world_size;
    networkConstStruct.nCompartments_l = 0;
    networkConstStruct.nChannels_l = 0;
    networkConstStruct.nGates_l = 0;

    networkConstStruct.nCells_u = sim.population.neuron_models.size();
    networkConstStruct.nCompartments_u = 0;
    networkConstStruct.nChannels_u = 0;
    networkConstStruct.nGates_u = 0;

    //-->>LOCAL NODE calculate the part it needs to calculate :D (substitude calculate with simulate in the second case)
    {
        size_t count = 0;
        size_t count2 = 0;
        for (size_t k = 0; k < (ProcessInfo.world_rank * networkConstStruct.nCells_l + networkConstStruct.nCells_l); k++) {
            const NeuronInfo &neuron = sim.population.neuron_models[count];
            if ((count2 != 0 && count2 % (neuron.multiplier - 1) == 0) || neuron.multiplier == 1) {
                count++;
                count2 = 0;
                if (k >= ProcessInfo.world_rank * networkConstStruct.nCells_l){
                    networkConstStruct.nCompartments_u += neuron.series_compartments.size();
                    for(const auto & series_compartment : neuron.series_compartments){
                        networkConstStruct.nChannels_u +=  series_compartment.ion_channels.size();
                        for(const auto & ion_channel : series_compartment.ion_channels){
                            networkConstStruct.nGates_u += ion_channel.multiplied_gates.size();
                        }
                    }
                }
            } else {
                count2++;
            }
            if (k < ProcessInfo.world_rank * networkConstStruct.nCells_l) continue;

            networkConstStruct.nCompartments_l += neuron.series_compartments.size();
            for (const auto & comp : neuron.series_compartments) {
                networkConstStruct.nChannels_l+= comp.ion_channels.size();
                for (const auto & chann : comp.ion_channels) {
                    networkConstStruct.nGates_l += chann.multiplied_gates.size();
                }
            }
        }
        if(count2){
            const NeuronInfo &neuron = sim.population.neuron_models[count];
            networkConstStruct.nCompartments_u += sim.population.neuron_models[0].series_compartments.size();
            for (size_t ii = 0; ii < sim.population.neuron_models[0].series_compartments.size(); ii++) {
                networkConstStruct.nChannels_u += sim.population.neuron_models[0].series_compartments[ii].ion_channels.size();
                for (size_t jj = 0; jj < sim.population.neuron_models[0].series_compartments[ii].ion_channels.size(); jj++) {
                    networkConstStruct.nGates_u += neuron.series_compartments[ii].ion_channels[jj].multiplied_gates.size();
                }
            }
        }
    }

    networkConstStruct.CellID_Offset = ProcessInfo.world_rank * networkConstStruct.nCells_l;
    log(LOG_DEBUG) << "Global numbers:\t\t\t(ORDEREDBLOCK: "<<ORDEREDBLOCK <<")"       << LOG_ENDL;
    log(LOG_DEBUG,0) << "  neurons:      "  << networkConstStruct.nCells          << LOG_ENDL;
    log(LOG_DEBUG,0) << "Local numbers:  "                                        << LOG_ENDL;
    log(LOG_DEBUG,0) << "  neurons:      " << networkConstStruct.nCells_l         << LOG_ENDL;
    log(LOG_DEBUG,0) << "  compartments: " << networkConstStruct.nCompartments_l  << LOG_ENDL;
    log(LOG_DEBUG,0) << "  channels:     " << networkConstStruct.nChannels_l      << LOG_ENDL;
    log(LOG_DEBUG,0) << "  gates:        " << networkConstStruct.nGates_l         << LOG_ENDL;
    log(LOG_DEBUG,0) << " Unique      :  "                                        << LOG_ENDL;
    log(LOG_DEBUG,0) << "  neurons:      " << networkConstStruct.nCells_u         << LOG_ENDL;
    log(LOG_DEBUG,0) << "  compartments: " << networkConstStruct.nCompartments_u  << LOG_ENDL;
    log(LOG_DEBUG,0) << "  channels:     " << networkConstStruct.nChannels_u      << LOG_ENDL;
    log(LOG_DEBUG,0) << "  gates:        " << networkConstStruct.nGates_u         << LOG_ENDL;

    //check padding
    if(ORDEREDBLOCK){
        if ((networkConstStruct.nCells_l) % BLOCKSIZE) {
            log(LOG_ERR) << "padding cells incorrect" << LOG_ENDL;
            fprintf(stderr, "padding cells incorrect\n");
            exit(2);
        }
    }

    // split cell's over GPU's
    if(ProcessInfo.GPU)
    {
        networkConstStruct.GPU_numbers = new GPU_number_obj[ProcessInfo.GPU];
        size_t AccummulationOfCells = 0;
        size_t SeenOfCells = 0;
        for(int i = 0; i < ProcessInfo.GPU; i++) {
            //initialize
            networkConstStruct.GPU_numbers[i].nCells_gpu = 0;
            networkConstStruct.GPU_numbers[i].nCompartments_gpu = 0;
            networkConstStruct.GPU_numbers[i].nChannels_gpu = 0;
            networkConstStruct.GPU_numbers[i].nGates_gpu = 0;

            if(i)  networkConstStruct.GPU_numbers[i].nCells_gpu =  networkConstStruct.nCells_l / ProcessInfo.GPU;
            else   networkConstStruct.GPU_numbers[i].nCells_gpu =  networkConstStruct.nCells_l / ProcessInfo.GPU + networkConstStruct.nCells_l % ProcessInfo.GPU;
            networkConstStruct.GPU_numbers[i].CellID_Offset_gpu = SeenOfCells;

            AccummulationOfCells += networkConstStruct.GPU_numbers[i].nCells_gpu;
             //no find the amount of things
            size_t count = 0;
            size_t count2 = 0;
            for (size_t k = 0; k < (ProcessInfo.world_rank * networkConstStruct.nCells_l + AccummulationOfCells); k++) {
                const NeuronInfo &neuron = sim.population.neuron_models[count];
                if ((count2 != 0 && count2 % (neuron.multiplier - 1) == 0) || neuron.multiplier == 1) {
                    count++;
                    count2 = 0;
                } else {
                    count2++;
                }
                if (k < ProcessInfo.world_rank * networkConstStruct.nCells_l + SeenOfCells) continue;

                networkConstStruct.GPU_numbers[i].nCompartments_gpu += neuron.series_compartments.size();
                for (const auto & comp : neuron.series_compartments) {
                    networkConstStruct.GPU_numbers[i].nChannels_gpu += comp.ion_channels.size();
                    for (const auto & chann : comp.ion_channels) {
                        networkConstStruct.GPU_numbers[i].nGates_gpu += chann.multiplied_gates.size();
                    }
                }
            }
            SeenOfCells += networkConstStruct.GPU_numbers[i].nCells_gpu;

            log(LOG_DEBUG,0) << " GPU id:  "       << i                                                    << LOG_ENDL;
            log(LOG_DEBUG,0) << "  neurons:      " << networkConstStruct.GPU_numbers[i].nCells_gpu         << LOG_ENDL;
            log(LOG_DEBUG,0) << "  compartments: " << networkConstStruct.GPU_numbers[i].nCompartments_gpu  << LOG_ENDL;
            log(LOG_DEBUG,0) << "  channels:     " << networkConstStruct.GPU_numbers[i].nChannels_gpu      << LOG_ENDL;
            log(LOG_DEBUG,0) << "  gates:        " << networkConstStruct.GPU_numbers[i].nGates_gpu         << LOG_ENDL;
            if(ORDEREDBLOCK){
                if ((networkConstStruct.GPU_numbers[i].nCells_gpu) % BLOCKSIZE) {
                    log(LOG_ERR) << "padding cells incorrect" << LOG_ENDL;
                    fprintf(stderr, "padding cells incorrect\n");
                    exit(2);
                }
            }

        }
    }

#ifdef DEBUG
#ifdef USE_MPI
    MPI_CHECK_RETURN(MPI_Barrier(MPI_COMM_WORLD));
#endif
    log(LOG_DEBUG) << "  offsetID:     " << networkConstStruct.CellID_Offset   << LOG_ENDL;
#ifdef USE_MPI
    MPI_CHECK_RETURN(MPI_Barrier(MPI_COMM_WORLD));
#endif
#else
    log(LOG_DEBUG) << "  offsetID:     " << networkConstStruct.CellID_Offset   << LOG_ENDL;
#endif
    return true;
}
bool fn_Allocate_network_state_host(NetworkConstStruct &Network_l,Network_state &NetworkState){
    INIT_LOG(LOG_DEFAULT);

    /* NETWORKSTATE */
    //compartments potentials
    NetworkState.hostVs       = new float[Network_l.nCompartments_l];
    NetworkState.hostVs_print = new float[Network_l.nCompartments_l];
    //compartments calcium concentration
    NetworkState.hostCalc       = new float[Network_l.nCompartments_l];
    NetworkState.hostCalc_print = new float[Network_l.nCompartments_l];
    //Channel currents
    NetworkState.hostCurrents       = new float[Network_l.nChannels_l];
    NetworkState.hostCurrents_print = new float[Network_l.nChannels_l];
    //activation variables
    NetworkState.hostYs        = new float[Network_l.nGates_l];
    NetworkState.hostYs_print  = new float[Network_l.nGates_l];
    //Igap placeholder to use between kernels
    NetworkState.Igap_host  = new float[Network_l.nCompartments_l];
    //Gap potential Placeholder
    NetworkState.VS_GAP_host = new float[Network_l.nCells];

    /* NETWORK CONSTANTS */
    Network_l.CellID_match_compID = new uint32_t[Network_l.nCompartments_l];
    Network_l.CompartmentIndex    = new uint32_t[Network_l.nCompartments_l];
    Network_l.ChannelIndex        = new uint32_t[Network_l.nCompartments_l];
    Network_l.GateIndex           = new uint32_t[Network_l.nCompartments_l];
    Network_l.Comp_index_vs_gates = new uint32_t[Network_l.nGates_l];
    Network_l.GateNO_vs_GateIndex = new uint32_t[Network_l.nGates_l];

    Network_l.G_int                   = new float[Network_l.nCells_l];
    Network_l.G_Channels              = new float[Network_l.nChannels_l];
    Network_l.Inverpotential_Channels = new float[Network_l.nChannels_l];
    Network_l.PassiveLeakConductivity = new float[Network_l.nCompartments_l];

    Network_l.CellPopulation                                   = new CellConstStruct[Network_l.nCells_l];
    Network_l.CellPopulation[0].compartment                    = new CompartmentConstStruct[Network_l.nCompartments_u];
    Network_l.CellPopulation[0].compartment[0].Channel         = new ChannelConstStruct[Network_l.nChannels_u];
    Network_l.CellPopulation[0].compartment[0].Channel[0].Gate = new GateConstStruct[Network_l.nGates_u];

    return true;
}
bool init_network_state(SimRunInfo sim,NetworkConstStruct &Network_l,Network_state &NetworkState) {
    INIT_LOG(LOG_WARN);
    log(LOG_DEBUG) << LOG_ENDL;

    //init the randomizer
    const uint32_t sample_scale = (1 << 24); //how many bits wide should a random sample be?
    const uint32_t max_rand_value = 16777215;
    XorShiftMul rng(((uint64_t) sim.population.neuron_models[0].random_seed << 32) + sim.population.neuron_models[0].random_seed);

    //checko if population is set
    if (sim.population.type != CellPopulationInfo::SET) {
        log(LOG_ERR) << "UNSET population type" << LOG_ENDL;
        exit(3);
    }

    //Initial state initializations
    {
        size_t count = 0;
        size_t count2 = 0;

        //walk trough network to find the specific corosponding cells !
        for (size_t i = 0; i < Network_l.CellID_Offset; i++) {
            if ((count2 != 0 && count2 % (sim.population.neuron_models[count].multiplier - 1) == 0) || sim.population.neuron_models[count].multiplier == 1) {
                count++;
                count2 = 0;
            } else {
                count2++;
            }
        }

        //helper variables
        uint32_t t_compCount = 0;
        uint32_t t_compCount2 = 0;
        uint32_t t_gateCount = 0;
        uint32_t t_channelcount = 0;
        uint32_t t_comp_offset = 0;
        uint32_t t_gate_offset = 0;
        uint32_t t_chan_offset = 0;
        uint32_t t_gate_offset_counter = 0;
        uint32_t t_chan_offset_counter = 0;

        //Find the gate and channel offsets.
        NeuronInfo *neuron_temp2 = &sim.population.neuron_models[0];
        for (size_t q = 0; q < neuron_temp2->series_compartments.size(); q++) {
            CompartmentInfo *comp_temp = &neuron_temp2->series_compartments[q];
            t_chan_offset_counter += comp_temp->ion_channels.size();
            for (size_t r = 0; r < comp_temp->ion_channels.size(); r++) {
                ChannelInfo *chan_temp = &comp_temp->ion_channels[r];
                t_gate_offset_counter += chan_temp->multiplied_gates.size();
            }
        }

        for (size_t i = 0; i < Network_l.nCells_l; i++) {

            size_t t_ccgate = 0;
            size_t t_channelcount2 = 0;
            NeuronInfo &neuron = sim.population.neuron_models[count];
//            std::cout << "new cell !! : " << i<<"\n";

            if(ORDEREDBLOCK && ((i % BLOCKSIZE) == 0) && i > 0) {
                t_comp_offset += (neuron.series_compartments.size()-1) * BLOCKSIZE;
                t_gate_offset += (t_gate_offset_counter-1) * BLOCKSIZE;
                t_chan_offset += (t_chan_offset_counter-1) * BLOCKSIZE;
            }

            //todo description
            if ((count2 != 0 && count2 % (neuron.multiplier - 1) == 0) || neuron.multiplier == 1) {
                count++;
                rng.update(((uint64_t) sim.population.neuron_models[count].random_seed << 32) + sim.population.neuron_models[count].random_seed);
                if (i != (Network_l.nCells_l - 1)) {
                    t_gate_offset_counter = 0;
                    t_chan_offset_counter = 0;
                    NeuronInfo *neuron_temp = &sim.population.neuron_models[count];
                    for (size_t q = 0; q < neuron_temp->series_compartments.size(); q++) {
                        CompartmentInfo *comp_temp = &neuron_temp->series_compartments[q];
                        t_chan_offset_counter += comp_temp->ion_channels.size();
                        for (size_t r = 0; r < comp_temp->ion_channels.size(); r++) {
                            ChannelInfo *chan_temp = &comp_temp->ion_channels[r];
                            t_gate_offset_counter += chan_temp->multiplied_gates.size();
                        }
                    }
                }
                count2 = 0;
            } else {
                count2++;
            }

            NetworkState.VS_GAP_host[i + Network_l.CellID_Offset] = neuron.series_compartments[0].initial_voltage;  //tempVS!!!
            NetworkState.Igap_host[i] = 0;

            //randomize the G_int for each cell
            const float G_Int = neuron.series_compartments[0].gp + ((float) (rng.Get() % sample_scale) / (float) (max_rand_value / 2) - 1) * neuron.Ratio_Random_offset;
            Network_l.G_int[i] = G_Int;

            //loop over all compartments
            for (size_t j = 0; j < neuron.series_compartments.size(); j++) {

                if(ORDEREDBLOCK){
                    t_compCount    = i + j * BLOCKSIZE + t_comp_offset;
                    t_channelcount = i + (t_channelcount2 * BLOCKSIZE) + t_chan_offset;
                    t_gateCount    = i + (t_ccgate * BLOCKSIZE) + t_gate_offset;
//                    std::cout << "number for now compartment:  c g ch " << t_compCount << "\t" << t_channelcount  << "\t" << t_gateCount << "\n";
                }

                //Initialize comparment voltages
                NetworkState.hostVs[t_compCount] = neuron.series_compartments[j].initial_voltage;

                //initialize localizers with relation to Compartment INDEX
                Network_l.CellID_match_compID[t_compCount] = i;
                Network_l.GateIndex[t_compCount] = t_gateCount;
                Network_l.ChannelIndex[t_compCount] = t_channelcount;

                //Randomize passive leakConductivity
                //or read out of file !
                float Gleak;
                if(neuron.enable_custom_leak) {
                    Gleak = neuron.custom_leak_array[j];
                }
                else{
                    Gleak = neuron.series_compartments[j].passive_leak_conductivity +
                                            ((float) (rng.Get() % sample_scale) / (float) (max_rand_value / 2) - 1) * neuron.series_compartments[j].passive_leak_conductivity_Randomization_offset;
                }
                Network_l.PassiveLeakConductivity[t_compCount] = Gleak;

                //Initialize CalciumConcentrations
                if (neuron.series_compartments[j].enable_CalciumConcentration) {
                    NetworkState.hostCalc[t_compCount] = neuron.series_compartments[j].CalciumConcentration.initial_value;
                } else {
                    NetworkState.hostCalc[t_compCount] = 0;
                }

                //loop over all channels
                for (size_t k = 0; k < neuron.series_compartments[j].ion_channels.size(); k++) {

                    if(ORDEREDBLOCK){
                        t_channelcount =  i + (t_channelcount2) * BLOCKSIZE + t_chan_offset;
                    }

                    //randomizations
                    const float Inverpotential_Channel = neuron.series_compartments[j].ion_channels[k].leak_inversion_potential +
                                                         ((float) (rng.Get() % sample_scale) / (float) (max_rand_value / 2) - 1) * neuron.series_compartments[j].ion_channels[k].leak_inversion_potential_Randomization_offset;
                    const float G_Channel = neuron.series_compartments[j].ion_channels[k].leak_conductivity +
                                            ((float) (rng.Get() % sample_scale) / (float) (max_rand_value / 2) - 1) * neuron.series_compartments[j].ion_channels[k].leak_conductivity_Randomization_offset;
                    Network_l.Inverpotential_Channels[t_channelcount] = Inverpotential_Channel;
                    Network_l.G_Channels[t_channelcount] = G_Channel;

                    //initialize currents
                    NetworkState.hostCurrents[t_channelcount] = 0; //Never needed for calculations

                    for (size_t l = 0; l < neuron.series_compartments[j].ion_channels[k].multiplied_gates.size(); l++) {
                        if(ORDEREDBLOCK){
                            t_gateCount = i + (l+t_ccgate) * BLOCKSIZE + t_gate_offset;
                        }

                        //Initialize activationVariables
                        NetworkState.hostYs[t_gateCount] = neuron.series_compartments[j].ion_channels[k].multiplied_gates[l].initial_value;

                        //to match gateIndex to compindex
                        Network_l.Comp_index_vs_gates[t_gateCount] = t_compCount;

                        //Increment gatecount
                        t_gateCount++;
                    }
                    t_ccgate += neuron.series_compartments[j].ion_channels[k].multiplied_gates.size();
                    t_channelcount++;
                    t_channelcount2++;
                }
                t_compCount++;
                t_compCount2++;
            }
        }
    }

    log(LOG_DEBUG) << "            ~finish" << LOG_ENDL;
    return true;
}
//todo explain
bool Initialize_Balanced_constant_state(SimRunInfo sim, process_info ProcessInfo,NetworkConstStruct &Network_l,Network_state &NetworkState_l){
    //IF GPU plz allocate differently :D Or allocate the same and copy all ??
    if(!fn_network_balancer(sim,ProcessInfo,Network_l)) return false;
    if(!fn_Allocate_network_state_host(Network_l,NetworkState_l)) return false;
    if(!init_network_state(sim,Network_l,NetworkState_l)) return false;
    return true;
}

bool fn_cellStructure_Initializations(SimRunInfo sim,NetworkConstStruct *Network_l_host,CellConstStruct *cellConstStruct_dev, CompartmentConstStruct *compartmentConstants_dev, ChannelConstStruct *channelConstants_dev,GateConstStruct *gateConstants_dev, size_t fromCellID, size_t aCellID)
{

    //    INIT_LOG(LOG_DEFAULT);

    //main variables
    CellConstStruct        *cellConstStruct             = Network_l_host->CellPopulation;
    CompartmentConstStruct *compartmentConstants        = Network_l_host->compartment;
    ChannelConstStruct     *channelConstants            = Network_l_host->Channel;
    GateConstStruct        *gateConstants               = Network_l_host->Gate;

    //helper variable,, wat een rotzooi ->)
    uint32_t count = 0;
    uint32_t count2 = 0;
    uint32_t channel_count = 0;
    uint32_t channel_count_2 = 0;
    uint32_t gate_count = 0;
    uint32_t gate_count_2 = 0;
    uint32_t gate_count_3 = 0;
    uint32_t gate_count_22 = 0;
    uint32_t comp_count = 0;
    uint32_t comp_count_2 = 0;
    uint32_t gate_offset_counter = 0;
    uint32_t chan_offset_counter = 0;
    uint32_t comp_offset = 0;
    uint32_t gate_offset = 0;

    //init the offsetcounters
    NeuronInfo *neuron_temp2 =  &sim.population.neuron_models[0];
    for(size_t q = 0; q <neuron_temp2->series_compartments.size(); q++){
        CompartmentInfo *comp_temp = &neuron_temp2->series_compartments[q];
        chan_offset_counter += comp_temp->ion_channels.size();
        for(size_t r = 0; r < comp_temp->ion_channels.size(); r++){
            ChannelInfo *chan_temp = &comp_temp->ion_channels[r];
            gate_offset_counter += chan_temp->multiplied_gates.size();
        }
    }


    //make sure to select the right neurons !
    for (size_t i = 0; i < fromCellID; i++) {
        NeuronInfo neuron = sim.population.neuron_models[count];
        if ((count2 != 0 && count2 % (neuron.multiplier - 1) == 0) || neuron.multiplier == 1) {
            count++;
            if (i != (aCellID - 1)) {
                gate_offset_counter = 0;
                chan_offset_counter = 0;
                NeuronInfo *neuron_temp = &sim.population.neuron_models[count];
                for (auto & series_compartment : neuron_temp->series_compartments) {
                    CompartmentInfo *comp_temp = &series_compartment;
                    chan_offset_counter += comp_temp->ion_channels.size();
                    for (auto & ion_channel : comp_temp->ion_channels) {
                        ChannelInfo *chan_temp = &ion_channel;
                        gate_offset_counter += chan_temp->multiplied_gates.size();
                    }
                }
            }
            count2 = 0;
        } else {
            count2++;
        }
    }

    //--->Cells
    for (size_t i = 0; i < aCellID; i++) {
        uint32_t ccgate = 0;
        uint32_t gate_fixer = 0;
        NeuronInfo neuron = sim.population.neuron_models[count];

        //ORDEREDBLOCK fix
        if(ORDEREDBLOCK && ((i % BLOCKSIZE) == 0) && i > 0) {
            comp_offset += (neuron.series_compartments.size() - 1) * BLOCKSIZE;
            gate_offset += (gate_offset_counter - 1) * BLOCKSIZE;

        }

        //get the offset right
        if ((count2 != 0 && count2 % (neuron.multiplier - 1) == 0) || neuron.multiplier == 1) {
            count++;
            if (i != (aCellID - 1)) {
                gate_offset_counter = 0;
                chan_offset_counter = 0;
                NeuronInfo *neuron_temp = &sim.population.neuron_models[count];
                for (auto & series_compartment : neuron_temp->series_compartments) {
                    CompartmentInfo *comp_temp = &series_compartment;
                    chan_offset_counter += comp_temp->ion_channels.size();
                    for (auto & ion_channel : comp_temp->ion_channels) {
                        ChannelInfo *chan_temp = &ion_channel;
                        gate_offset_counter += chan_temp->multiplied_gates.size();
                    }
                }
            }
            count2 = 0;
        } else {
            count2++;
        }

        float gInt = neuron.series_compartments[0].gp;

        cellConstStruct[i].compartment = &compartmentConstants_dev[comp_count_2];
        //--->Compartment
        for (size_t c = 0; c < neuron.series_compartments.size(); c++) {

            if(ORDEREDBLOCK) comp_count = i + c * BLOCKSIZE + comp_offset;

            Network_l_host->CompartmentIndex[comp_count] = c + comp_count_2; //compcount matchen aan de compartmentINDEX

            if (count2 == 0 || i == (aCellID - 1)) {
                GateConstStruct calCon;
                if (neuron.series_compartments[c].enable_CalciumConcentration) {
                    const GateInfo gate = neuron.series_compartments[c].CalciumConcentration;
                    float xsA[9];
                    float xsB[9];

                    FormulaToXsN(gate.alpha_formula, xsA);
                    FormulaToXsN(gate.beta_formula, xsB);

                    int32_t bbft = 0;
                    if (gate.current_gating_variable == GateInfo::GATE_BY_BETA) bbft = 12;
                    int32_t aaft = 0;
                    if (gate.value_dynamics == GateInfo::DYNAMICS_RATIO) aaft = 4;
                    if (gate.value_dynamics == GateInfo::DYNAMICS_CONCENTRATED_CALCIUM) aaft = 8;
                    uint32_t alphaFType = ((gate.alpha_formula.type - 1) & mask1) + (aaft);
                    uint32_t betaFType = ((gate.beta_formula.type - 1) & mask1) + (bbft);

                    calCon = newGateConstStruct(xsA, xsB, alphaFType, betaFType, gate.gating_power_factor);
                }
                uint32_t Identifier;
                if (c == 0 && (c + 1) == neuron.series_compartments.size()) Identifier = 3;
                else if ((c + 1) == neuron.series_compartments.size()) Identifier = 2;
                else if (c == 0) Identifier = 0;
                else Identifier = 1;
                compartmentConstants[c + comp_count_2] = newCompartmentConstStruct(neuron.series_compartments[c].inverse_capacitance, neuron.series_compartments[c].passive_leak_inversion_potential,
                                                                                   neuron.series_compartments[c].passive_leak_conductivity, gInt, neuron.series_compartments[c].gp, Identifier,
                                                                                   neuron.series_compartments[c].ion_channels.size(),
                                                                                   &channelConstants_dev[channel_count_2], neuron.series_compartments[c].enable_CalciumConcentration, calCon);
            }
            //--->Channel
            for (size_t j = 0; j < neuron.series_compartments[c].ion_channels.size(); j++) {
                if (count2 == 0 || i == (aCellID - 1)) {
                    const ChannelInfo channel = neuron.series_compartments[c].ion_channels[j];
                    channelConstants[j + channel_count_2] = newChannelConstStruct(channel.is_ca2plus_channel, channel.multiplied_gates.size(), &gateConstants_dev[gate_count_2]);
                }

                //--->GATE
                for (size_t k = 0; k < neuron.series_compartments[c].ion_channels[j].multiplied_gates.size(); k++) {
                    if(ORDEREDBLOCK) gate_count = i + (k+ccgate) * BLOCKSIZE + gate_offset;
                    Network_l_host->GateNO_vs_GateIndex[gate_count] = gate_fixer + gate_count_22;


                    if (count2 == 0 || i == (aCellID - 1)) {
                        const GateInfo gate = neuron.series_compartments[c].ion_channels[j].multiplied_gates[k];
                        float xsA[9];
                        float xsB[9];
                        FormulaToXsN(gate.alpha_formula, xsA);
                        FormulaToXsN(gate.beta_formula, xsB);
                        int32_t bbft = 0;
                        if (gate.current_gating_variable == GateInfo::GATE_BY_BETA) bbft = 12;
                        int32_t aaft = 0;
                        if (gate.value_dynamics == GateInfo::DYNAMICS_RATIO) aaft = 4;
                        if (gate.value_dynamics == GateInfo::DYNAMICS_CONCENTRATED_CALCIUM) aaft = 8;
                        uint32_t alphaFType = ((gate.alpha_formula.type - 1) & mask1) + (aaft);
                        uint32_t betaFType = ((gate.beta_formula.type - 1) & mask1) + (bbft);
//                        log(LOG_DEBUG) << "new unique->: cell id =" << i << " + " << Network_l_host.CellID_Offset << ", Comartment: " << c + comp_count_2 << ", Channel: "<< j + channel_count_2 << " , Gate: " << k + gate_count_2 << LOG_ENDL;
                        gateConstants[k + gate_count_2] = newGateConstStruct(xsA, xsB, alphaFType, betaFType, gate.gating_power_factor);
//                        if(0) {
//                            printf("Gate %d\n", gate_count);
//                            printf("alphaType %d ", alphaFType);
//                            for (int ii = 0; ii < 9; ii++) {
//                                printf("xsA[%d] %.4f  ", ii, xsA[ii]);
//                            }
//                            printf("\n");
//                            printf("betaType %d ", betaFType);
//                            for (int ii = 0; ii < 9; ii++) {
//                                printf("xsB[%d] %.4f  ", ii, xsB[ii]);
//                            }
//                            /*are randomized
//                            printf("\n");
//                            printf("p %f\tg %f\tvChannel %f\tyInit %f\n", (float) gate.gating_power_factor, g, vChannel, gate.initial_value);
//                            printf("\n");*/
//                        }
                    }
                    gate_fixer++;
                    gate_count++;
                } //---> gates afsluiten
                ccgate += neuron.series_compartments[c].ion_channels[j].multiplied_gates.size();
                gate_count_3++;
                if (count2 == 0 || i == (aCellID - 1)) {
                    gate_count_2 += neuron.series_compartments[c].ion_channels[j].multiplied_gates.size();
                }
                channel_count++;
            } //--->Channels afsluiten
            if (count2 == 0 || i == (aCellID - 1)) {
                channel_count_2 += neuron.series_compartments[c].ion_channels.size();
            }
            comp_count++;
        } //---> comparment afsluiten
        if (count2 == 0 || i == (aCellID - 1)) {
            comp_count_2 += neuron.series_compartments.size();
            gate_count_22 = gate_count_2;
        }
    } //---> cells afsluiten

    return true;
}
